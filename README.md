# A C64 Assembly programming playground

The development tools I use:

* [Kick Assembler](http://www.theweb.dk/KickAssembler/Main.html)
* Sublime Text with [Kick Assembler extension](https://packagecontrol.io/packages/Kick%20Assembler%20(C64))
* C64 [Vice Emulator](https://vice-emu.sourceforge.io/)
* [GoatTracker 2](https://sourceforge.net/projects/goattracker2/) SID music editor

## files
sprite_demo.asm - A very simple example from [8-Bit Show and Tell](https://www.youtube.com/channel/UC3gRBswFkuteshdwMZAQafQ)
