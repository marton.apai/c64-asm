		.var v = $d000  

		* = $4000	// starting memory address ( -> run the program with sys 16384 )

		lda #147  	// control character "clear"
		jsr $ffd2 	// clears screen

		lda #3
		sta $d020

		lda #13		// sprite definition 13
		sta $7f8	// =2040, the default location of sprite definition
		lda #1
		sta v+21	// sprite enable register
		sta v+39	// sprite color register (8 bytes, 1 for each sprite)

		ldx #62
		lda #255
loop:	sta 832,x
		dex
		bpl loop

forever:
		lda #$ff
		cmp $d012 	// raster counter
		bne forever

up:		lda 56320
		and #1
		bne down
		dec spry
down:	lda 56320
		and #2
		bne left
		inc spry
left:	lda 56320
		and #4
		bne right
		dec sprx
		lda sprx
		cmp #$ff
		bne right
		dec sprx+1

right:	lda 56320
		and #8
		bne button
		inc sprx
		bne	button
		inc sprx+1

button:	lda 56320
		and #16
		bne done
		inc $d020
done:
		lda sprx
		sta v

		lda sprx+1
		sta v+16		// HACK! This byte has the high bit of all sprites

		lda spry
		sta v+1
		jmp forever

sprx:	.byte 100,0
spry:	.byte 100